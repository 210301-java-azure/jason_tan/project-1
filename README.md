# Project 1

## Description

This web application is a simple electronic medical record system for the use of storing and retrieving basic patient information. 

## Technologies Used
- Front-end
    - HTML
    - CSS
    - Javascript
    - Azure Blob Storage
- Back-end
    - Java
        - Javalin
        - Hibernate ORM
    - Azure SQL Server database
    - Azure Linux Virtual Machine
- DevOps
    - Jenkins CI/CD automation pipeline

## Features
- Register new user accounts
- Login with existing user accounts
- Enter new patient information
- Search existing patient information
    - by first and last name
    - by patient id
- Edit patient information
- Delete patient information

- Possible features to add on
    - Storing appointment information
        - Could show past and upcoming appointments
    - Storing prescription information

## How to use

1. Begin by downloading the code to your local system using `git clone https://gitlab.com/210301-java-azure/jason_tan/project-1.git`

2. Run a database locally or through cloud services such as Microsoft Azure or AWS. 

3. To run the back-end server:
    - The following lines of code in `src/main/java/dev/tan/util/HibernateUtil.java` must be configured with your own database credentials
        ```java
        settings.put(Environment.URL, System.getenv("DB_URL"));
        settings.put(Environment.USER, System.getenv("DB_USER"));
        settings.put(Environment.PASS, System.getenv("DB_PASS"));
        ```
    - Build the program using `./gradlew finalJar` in a command-line terminal
    - Run the resulting jar file with `java -jar build/libs/EMR-fullstack-1.0-SNAPSHOT.jar`

4. To use the front-end webpages:
    - Go through each *.js file and change the host of the Fetch requests to `localhost`. Example:
        ```js
        Before:
        fetch("http://13.72.71.180:7000/register", {
            
        After:
        fetch("http://localhost:7000/register", {
        ```

