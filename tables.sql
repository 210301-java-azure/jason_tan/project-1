create table users (
	id serial primary key,
	username varchar(30),
	password varchar(30),
	role int -- 1 = admin, 2 = patient
);

insert into users (username, password, role) values
	('jasontan', 'asdf123', 1)

delete from users;
drop table users;

create table patients (
	id serial primary key,
	uid int references users (id),
	firstName varchar(30),
	lastName varchar(30),
	sex varchar(1),
	DOB varchar(10),
	race varchar(20),
	address varchar(50),
	city varchar(30),
	state varchar(2),
	phone varchar(15),
	insuranceName varchar(30),
	insurancePolicyNumber int,
	insuranceGroupID int
);

delete from patients;
drop table patients;
