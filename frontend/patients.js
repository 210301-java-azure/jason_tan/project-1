
var searchForm = document.getElementById("search-form")
searchForm.onchange = e => {
	var form = new FormData(searchForm);
	var nameInputs = document.getElementById("name-inputs");
	var idInput = document.getElementById("id-input");
	switch(form.get("searchMode")) {
		case "all":
			nameInputs.hidden = true;
			idInput.hidden = true;
			break;
		case "name":
			nameInputs.hidden = false;
			idInput.hidden = true;
			break;
		case "id":
			nameInputs.hidden = true;
			idInput.hidden = false;
			break;
	}
}

searchForm.onsubmit = e => {
	e.preventDefault();
	var form = new FormData(searchForm);
	var searchMode = form.get("searchMode");
	var url = "http://13.72.71.180:7000/patients/search/"+searchMode;
	switch(searchMode) {
		case "name":
			var lastName = form.get("lastName");
			var firstName = form.get("firstName");
			if(!lastName || !firstName){
				window.alert("You must provide last and first name");
				return;
			}
			url += `/${lastName}/${firstName}`;
			break;
		case "id":
			var id = form.get("id");
			if(!id) {
				window.alert("You must provide an id");
				return;
			}
			url += `/${id}`;
			break;
		default:
			break;
	}
	fetch(url, {
		method: "get",
		mode: "cors",
		headers: new Headers({
			"Authorization": sessionStorage.getItem("token")
		})
	}).then(async res => {
		if(res.status == 200) {
			var data = await res.json();
			console.log(data);
			setSearchResults(data);
		}
		else if(res.status == 404) {
			var err = document.getElementById("search-err")
			err.innerHTML = "No patients found"
			setTimeout(() => {
				err.innerHTML = ""
			}, 5000)
		}
	}, rej => {
		console.log(rej);
	})
}

function setSearchResults(data) {
	document.getElementById("search-results").hidden = false;
	var table = document.getElementById("search-body");

	table.querySelectorAll("tr").forEach(node => {
		table.removeChild(node);
	})
	data.forEach(p => {
		var tr = document.createElement("tr");
		var td1 = document.createElement("td");
		var td2 = document.createElement("td");
		var td3 = document.createElement("td");

		td1.textContent = p.uid;
		td2.textContent = p.lastName;
		td3.textContent = p.firstName;

		tr.appendChild(td1);
		tr.appendChild(td2);
		tr.appendChild(td3);
		tr.className = "result";
		tr.onclick = () => setDataPanel(p);
		table.appendChild(tr);
	})
}

function setDataPanel(data) {
	console.log(data);
	document.getElementById("init-text").hidden = true;
	var form = document.getElementById("patient-data-form");

	for(var e in data) {
		form.querySelector("#"+e).value = data[e]
	}

	document.getElementById("patient-data-container").hidden = false;
}

document.getElementById("patient-data-form").onsubmit = e => {
	e.preventDefault();
}

document.getElementById("save-patient-btn").onclick = e => {
	console.log("saving patient data");
	editOrDelete("edit")
}

document.getElementById("delete-patient-btn").onclick = e => {
	console.log("deleting patient data");
	editOrDelete("delete")
}

function editOrDelete(mode) {
	var url = "";
	var method = "";
	var successMsg = "";
	if(mode == "edit") {
		url = "http://13.72.71.180:7000/patients/edit/";
		method = "post";
		successMsg = "Patient info edited successfully"
	}
	else if(mode == "delete") {
		url = "http://13.72.71.180:7000/patients/delete/";
		method = "delete"
		successMsg = "Patient info deleted successfully"
	}
	var formdata = new FormData(document.getElementById("patient-data-form"));
	var formObj = {};
	formdata.forEach((e,k) => {
		formObj[k] = e;
	})

	fetch(url, {
		method: method,
		body: JSON.stringify(formObj),
		headers: new Headers({
			"Authorization": sessionStorage.getItem("token")
		})
	}).then(res => {
		var err = document.getElementById("")
		var elem = document.getElementById("patient-err");
		switch(res.status) {
			case 200:
				console.log("patient info edited successfully");
				elem.innerHTML = successMsg;
				setTimeout(() => {
					elem.innerHTML = ""
				},5000)
				break;
			case 404:
				console.log("patient does not exist")
				break;
			default:
				console.log("Server Error")
				elem.innerHTML = "Server Error"
				setTimeout(() => {
					elem.innerHTML = ""
				},5000)
		}
	}, rej => {
		console.log(rej);
	})
}