document.getElementById("logout-link").addEventListener("click", (e) => {
	if(!window.confirm("Are you sure you want to logout?")) {
		return;
	}
	sessionStorage.removeItem("token");
	window.location.href = "landing.html"
})