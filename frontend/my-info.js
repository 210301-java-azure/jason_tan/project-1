var newPatient;

var token = sessionStorage.getItem("token");
if(token) {
	var uid = token.split(',')[1];
	fetch("http://13.72.71.180:7000/patients/search/id/"+uid, {
		method: "get",
		mode: "cors",
		headers: new Headers({
			"Authorization": sessionStorage.getItem("token")
		})
	}).then(res => {
		switch(res.status) {
			case 200:
				console.log("patient found");
				newPatient = false;
				res.json().then(r => {
					setDataPanel(r[0]);
				})
				break;
			case 404:
				console.log("patient not found");
				newPatient = true;
				document.getElementById("my-info-err").innerHTML = "No patient information found. Please enter your information"
				break;
			default:
				document.getElementById("my-info-err").innerHTML = "Server Error"
		}
	}, rej => {
		console.log(rej);
	})
}

function setDataPanel(data) {
	console.log(data);
	var form = document.getElementById("patient-data-form");

	for(var e in data) {
		form.querySelector("#"+e).value = data[e]
	}
}

document.getElementById("save-patient-btn").onclick = e => {
	console.log("saving patient data");
	editOrDelete("edit")
}

document.getElementById("delete-patient-btn").onclick = e => {
	console.log("deleting patient data");
	editOrDelete("delete")
}

function editOrDelete(mode) {
	var url = "";
	var method = "";
	var successMsg = "";
	if(mode == "edit") {
		if(newPatient) {
			url = "http://13.72.71.180:7000/patients/add/";
			successMsg = "Patient info added successfully"
		}
		else {
			url = "http://13.72.71.180:7000/patients/edit/";
			successMsg = "Patient info edited successfully"
		}
			method = "post";
	}
	else if(mode == "delete") {
		url = "http://13.72.71.180:7000/patients/delete/";
		method = "delete"
		successMsg = "Patient info deleted successfully"
	}
	var formdata = new FormData(document.getElementById("patient-data-form"));
	var formObj = {};
	formdata.forEach((e,k) => {
		if(newPatient && k == "id") return
		formObj[k] = e;
	})
	formObj["uid"] = sessionStorage.getItem("token").split(",")[1];
	console.log(formObj);
	fetch(url, {
		method: method,
		body: JSON.stringify(formObj),
		headers: new Headers({
			"Authorization": sessionStorage.getItem("token")
		})
	}).then(res => {
		var err = document.getElementById("")
		switch(res.status) {
			case 200:
				console.log(successMsg);
				if(newPatient) newPatient = false;
				
				var elem = document.getElementById("patient-err");
				elem.innerHTML = successMsg;
				setTimeout(() => {
					elem.innerHTML = ""
				},5000)
				break;
			case 404:
				console.log("patient does not exist")
				break;
			default:
				console.log("Server Error")
				var elem = document.getElementById("patient-err");
				elem.innerHTML = "Server Error"
				setTimeout(() => {
					elem.innerHTML = ""
				},5000)
		}
	}, rej => {
		console.log(rej);
	})
}