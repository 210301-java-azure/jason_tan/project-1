
function removeClass(elem, str) {
	elem.className = elem.className.split(" ").filter(e => e != str).join(" ");
}

function chooseMode(mode) {
	var login = document.querySelector("#login-tab");
	var register = document.querySelector("#register-tab");
	if(mode === "login") {
		if(!login.className.includes("choice-tab"))
			login.className += " choice-tab";
		removeClass(register, "choice-tab");
		document.getElementById("login-form").hidden = false;
		document.getElementById("register-form").hidden = true;
	}
	else if(mode === "register") {
		if(!register.className.includes("choice-tab"))
			register.className += " choice-tab";
		removeClass(login, "choice-tab");
		document.getElementById("login-form").hidden = true;
		document.getElementById("register-form").hidden = false;
	}
}

document.getElementById("login-form").addEventListener("submit", (e) => {
	e.preventDefault();
	console.log("logging in");
	const form = new FormData(document.getElementById("login-form"));
	fetch("http://13.72.71.180:7000/login", {
		method: "post",
		body: form
	}).then(async res => {
		console.log(res)
		switch(res.status) {
			case 200:
				sessionStorage.setItem("token", res.headers.get("Authorization"));
				console.log("logged in, going to landing");
				window.location.href = "landing.html"
				break;
			case 400:
				var err = document.getElementById("auth-err")
				err.innerHTML = "Incorrect username and password combination";
				setTimeout(() => {
					err.innerHTML = "";
				}, 5000);
				break;
			case 404:
				var err = document.getElementById("auth-err")
				err.innerHTML = "User not found";
				setTimeout(() => {
					err.innerHTML = "";
				}, 5000);
				break;
		}
	}, rej => {
		console.log(rej);
	})
})

var registerForm = document.getElementById("register-form");
registerForm.addEventListener("submit", (e) => {
	e.preventDefault();
	console.log("registering new account");
	const form = new FormData(document.getElementById("register-form"));
	fetch("http://13.72.71.180:7000/register", {
		method: "post",
		body: form
	}).then(res => {
		console.log(res);
		var err = document.getElementById("auth-err")
		if(res.status == 200) {
			err.innerHTML = "Account created successfully. Log in to continue";
		}
		else if(res.status == 400) {
			err.innerHTML = "Username already taken";
		}
		else {
			err.innerHTML = "Server Error";
		}
		setTimeout(() => {
			err.innerHTML = "";
		}, 5000);

	}, rej => {
		console.log(rej);
	})	
})
console.log("hello")
registerForm.querySelector('input[name="username"]').addEventListener("blur", e => {
	var input = registerForm.querySelector('input[name="username"]').value;
	var subBtn = registerForm.querySelector('input["submit"]')
	if(/^[a-zA-Z0-9_]{4,}$/.test(input)) {
		subBtn.disabled = false;
		input.style.border = "";
		return;
	}
	else {
		subBtn.disabled = true;
		input.style.border = "2px solid red";
		var elem = document.getElementById("auth-error")
		elem.innerHTML = "Invalid username";
		setTimeout(() => {
			elem.innerHTML = ""
		}, 4000)
	}
})

registerForm.querySelector('input[name="password"]').addEventListener("blur", e => {
	var input = registerForm.querySelector('input[name="password"]').value;
	var subBtn = registerForm.querySelector('input["submit"]')
	if(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/.test(input)) {
		subBtn.disabled = false;
		input.style.border = "";
		return;
	}
	else {
		subBtn.disabled = true;
		input.style.border = "2px solid red";
		var elem = document.getElementById("auth-error")
		elem.innerHTML = "Invalid password";
		setTimeout(() => {
			elem.innerHTML = ""
		}, 4000)
	}
})