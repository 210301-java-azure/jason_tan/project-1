package dev.tan.models;

import javax.persistence.*;

@Entity
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @JoinColumn(name="id")
    private int uid;
    private String firstName;
    private String lastName;
    private String sex;
    private String DOB;
    private String race;
    private String address;
    private String city;
    private String state;
    private String phone;
    private String insuranceName;
    private int insurancePolicyNumber;
    private int insuranceGroupID;

    public Patient() {}

    public Patient(int id, int uid, String firstName, String lastName, String sex, String DOB, String race, String address, String city, String state, String phone, String insuranceName, int insurancePolicyNumber, int insuranceGroupID) {
        this.uid = uid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
        this.DOB = DOB;
        this.race = race;
        this.address = address;
        this.city = city;
        this.state = state;
        this.phone = phone;
        this.insuranceName = insuranceName;
        this.insurancePolicyNumber = insurancePolicyNumber;
        this.insuranceGroupID = insuranceGroupID;
    }

    public Patient(int uid, String firstName, String lastName, String sex, String DOB, String race, String address, String city, String state, String phone, String insuranceName, int insurancePolicyNumber, int insuranceGroupID) {
        this.uid = uid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.sex = sex;
        this.DOB = DOB;
        this.race = race;
        this.address = address;
        this.city = city;
        this.state = state;
        this.phone = phone;
        this.insuranceName = insuranceName;
        this.insurancePolicyNumber = insurancePolicyNumber;
        this.insuranceGroupID = insuranceGroupID;
    }

    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    public int getInsurancePolicyNumber() {
        return insurancePolicyNumber;
    }

    public void setInsurancePolicyNumber(int insurancePolicyNumber) {
        this.insurancePolicyNumber = insurancePolicyNumber;
    }

    public int getInsuranceGroupID() {
        return insuranceGroupID;
    }

    public void setInsuranceGroupID(int insuranceGroupID) {
        this.insuranceGroupID = insuranceGroupID;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", uid=" + uid +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", sex='" + sex + '\'' +
                ", DOB='" + DOB + '\'' +
                ", race='" + race + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", phone='" + phone + '\'' +
                ", insuranceName='" + insuranceName + '\'' +
                ", insurancePolicyNumber=" + insurancePolicyNumber +
                ", insuranceGroupID=" + insuranceGroupID +
                '}';
    }
}
