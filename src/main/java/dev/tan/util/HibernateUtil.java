package dev.tan.util;

import dev.tan.models.*;
import org.hibernate.*;
import org.hibernate.cfg.*;

import java.util.Properties;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    private static SessionFactory getSessionFactory(){
        if(sessionFactory==null){
            Configuration configuration = new Configuration();
            Properties settings = new Properties();
            settings.put(Environment.URL, System.getenv("DB_URL"));
            settings.put(Environment.USER, System.getenv("DB_USER"));
            settings.put(Environment.PASS, System.getenv("DB_PASS"));

            settings.put(Environment.DRIVER, "com.microsoft.sqlserver.jdbc.SQLServerDriver");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.SQLServerDialect");

            settings.put(Environment.HBM2DDL_AUTO, "update");
            settings.put(Environment.SHOW_SQL, "true");

            configuration.setProperties(settings);

            //provide hibernate mappings to configuration
            configuration.addAnnotatedClass(Patient.class);
            configuration.addAnnotatedClass(User.class);

            sessionFactory = configuration.buildSessionFactory();
        }
        return sessionFactory;
    }

    public static Session getSession(){
        return getSessionFactory().openSession();
    }


}
