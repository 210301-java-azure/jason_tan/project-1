package dev.tan.controllers;

import dev.tan.data.*;
import dev.tan.models.User;
import io.javalin.core.util.Header;
import io.javalin.http.*;
import org.slf4j.*;

public class AuthController {

    private static Logger logger = LoggerFactory.getLogger(AuthController.class);
    private static DatabaseHib db = new DatabaseHib();

    public static void handlePreAuthCheck(Context ctx) {
        logger.info("checking for authorization");
        if(ctx.method().equals("OPTIONS"))
            return;

        String token = ctx.header("Authorization");
        logger.info(token);
        if(token == null || token.equals("")){
            logger.info("no token");
            //throw new UnauthorizedResponse("Not authorized to perform this action. Please log in and try again.");
            ctx.status(401);
            return;
        }
        ctx.header("Access-Control-Allow-Methods", "get, post, patch, delete");
    }

    public static void handleCreateAccount(Context ctx) {
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");
        String role = ctx.formParam("role");
        int res = db.addAccount(username, password, role);
        if(res > 0) {
            logger.info("Account created successfully");
            ctx.json("Account created successfully\nUser ID: "+res);
        }
        else {
            logger.info("username taken");
            //throw new BadRequestResponse("Username already taken");
            ctx.status(400);
        }
    }

    public static void handleLogin(Context ctx) {
        System.out.println(ctx.contentType());
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");
        if(username != null && username.equals("") ||
           password != null && password.equals("")) {
            //throw new BadRequestResponse();
            ctx.status(400);
            return;
        }
        logger.info("attempting logging in as: " + username);
        User user = db.getUser(username, password);
        if(user != null) {
            logger.info("logged in as " + user);

            ctx.header(Header.ACCESS_CONTROL_EXPOSE_HEADERS, "Authorization");
            ctx.header("Authorization",
                    (user.getRole() == 1 ? "doctor" : "patient") + "," + user.getId());
            ctx.json(user);
        }
        else {
            ctx.status(404);
        }
    }
}
