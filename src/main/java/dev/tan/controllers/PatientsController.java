package dev.tan.controllers;

import dev.tan.data.*;
import dev.tan.models.Patient;
import io.javalin.http.*;
import org.slf4j.*;

import java.util.List;

public class PatientsController {

    private static Logger logger = LoggerFactory.getLogger(PatientsController.class);
    private static DatabaseHib db = new DatabaseHib();

    public static void handleSearchPatients(Context ctx) {
        String mode = ctx.pathParam("searchMode");
        String[] token = ctx.header("Authorization").split(",");
        if(mode == null) {
            //throw new BadRequestResponse("Please provide a search parameter");
            ctx.status(400);
            return;
        }
        List<Patient> res = null;
        switch (mode) {
            case "all":
                if(!token[0].equals("doctor")) {
                    //throw new UnauthorizedResponse("This request requires admin privileges");
                    ctx.status(404);
                    return;
                }
                logger.info("getting all patients");
                res = db.searchPatients("all", "", "");
                break;
            case "name":
                if(!token[0].equals("doctor")) {
                    //throw new UnauthorizedResponse("This request requires admin privileges");
                    ctx.status(401);
                    return;
                }
                String lastName = ctx.pathParam("lastName");
                String firstName = ctx.pathParam("firstName");
                if(lastName == null || firstName == null) {
                    //throw new BadRequestResponse("Please provide a full search query.");
                    ctx.status(400);
                    return;
                }
                logger.info("searching for patients by name: " +
                        lastName + ", " + firstName);
                res = db.searchPatients("name", lastName, firstName);
                break;
            case "id":
                String id = ctx.pathParam("id");
                logger.info("searching for patient by id: " + id);
                res = db.searchPatients("id", id, "");
                break;
        }

        if(res.isEmpty()) {
            //throw new NotFoundResponse("No patients found");
            ctx.status(404);
            return;
        }
        else {
            if(token[0].equals("doctor")) ctx.json(res);
            else if(token[0].equals("patient")) {
                if(Integer.parseInt(token[1]) != res.get(0).getUid()) {
                    //throw new UnauthorizedResponse("Searching for information of other patients requires admin privileges");
                    ctx.status(401);
                    return;
                }
                ctx.json(res);
            }
        }
    }

    public static void handleAddPatient(Context ctx) {
        Patient p = ctx.bodyAsClass(Patient.class);
        System.out.println(p);
        String[] token = ctx.header("Authorization").split(",");
        if(token[0].equals("patient") && p.getUid() != Integer.parseInt(token[1])) {
            //throw new UnauthorizedResponse("Without admin privileges, you are only allowed to add information for yourself");
            ctx.status(401);
            return;
        }
        logger.info("adding patient");
        logger.info(p.toString());

        if(!db.addPatient(p)) {
            ctx.status(500);
            ctx.json("Could not add patient");
        }
        else {
            ctx.status(200);
            ctx.result("Added patient successfully");
        }
    }

    public static void handleEditPatient(Context ctx) {
        Patient p = ctx.bodyAsClass(Patient.class);
        logger.info("editing patient");
        if(db.editPatient(p))
            ctx.result("Patient information changed successfully");
        else {
            ctx.status(500);
            ctx.json("Could not change patient information");
        }
    }

    public static void handleDeletePatient(Context ctx) {
        Patient p = ctx.bodyAsClass(Patient.class);
        logger.info("deleting patient");
        if(db.deletePatient(p))
            ctx.result("Patient information deleted successfully");
        else {
            ctx.status(500);
            ctx.json("Could not delete patient information");
        }
    }
}
