package dev.tan.data;

import dev.tan.models.*;
import dev.tan.util.HibernateUtil;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.NotFoundResponse;
import org.hibernate.*;
import org.slf4j.*;
import java.util.*;

public class DatabaseHib {
    private static Logger logger = LoggerFactory.getLogger(DatabaseHib.class);

    public int addAccount(String username, String password, String role) {
        try(Session s = HibernateUtil.getSession()) {
            User u = s.createQuery("from Users where username = :user", User.class)
                    .setParameter("user", username).uniqueResult();
            if(u != null)
                return 0;

            Transaction t = s.beginTransaction();
            int id = (int) s.save(new User(username, password, (role.equals("doctor") ? 1:2)));
            t.commit();
            return id;
        }
    }

    public User getUser(String username, String password) {
        try(Session s = HibernateUtil.getSession()) {
            User u = s.createQuery("from Users where username = :user", User.class)
                    .setParameter("user", username).uniqueResult();
            if(u == null) {
                logger.warn("no user found");
                //throw new NotFoundResponse("No user found");
                return null;
            }
            if(!u.getPassword().equals(password)) {
                //throw new BadRequestResponse("Password incorrect");

            }

            return u;
        }
    }

    public boolean addPatient(Patient p) {
        try(Session s = HibernateUtil.getSession()) {
            Patient patient = s.createQuery("from Patient " +
                    "where lastName = :last and firstName = :first", Patient.class)
                    .setParameter("last", p.getLastName())
                    .setParameter("first", p.getFirstName())
                    .uniqueResult();
            if(patient != null) {
                //throw new BadRequestResponse("Patient already exists");
                return false;
            }

            logger.info("adding new patient");
            Transaction t = s.beginTransaction();
            int id = (int) s.save(p);
            p.setId(id);
            t.commit();
            return true;
        }
    }

    public List<Patient> searchPatients(String mode, String param1, String param2) {
        try(Session s = HibernateUtil.getSession()) {
            List<Patient> list = new LinkedList<>();
            switch(mode) {
                case "all":
                    list = s.createQuery("from Patient", Patient.class).list();
                    break;
                case "name":
                     list = s.createQuery("from Patient " +
                            "where lastName = :last and firstName = :first", Patient.class)
                             .setParameter("last", param1)
                             .setParameter("first", param2)
                             .list();
                    break;
                case "id":
                    list = s.createQuery("from Patient where " +
                            "uid = :uid", Patient.class)
                            .setParameter("uid", Integer.parseInt(param1)).list();
                    break;
            }
            return list;
        }
    }

    public boolean editPatient(Patient p) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction t = s.beginTransaction();
            s.merge(p);
            t.commit();
            return true;
        } catch(Exception e) {
            return false;
        }
    }

    public boolean deletePatient(Patient p) {
        try(Session s = HibernateUtil.getSession()) {
            Transaction t = s.beginTransaction();
            s.delete(p);
            t.commit();
            return true;
        }
        catch(Exception e) {
            return false;
        }
    }
}
