//package dev.tan.data;
//
//import dev.tan.models.Patient;
//import dev.tan.models.User;
//import io.javalin.http.*;
//import org.slf4j.*;
//import java.util.*;
//import java.sql.*;
//
//public class Database {
//    private static Logger logger = LoggerFactory.getLogger(Database.class);
//    private Connection con;
//
//    public Database() {
//        String url = System.getenv("url");
//        String username = System.getenv("username");
//        String password = System.getenv("password");
//        try {
//            con = DriverManager.getConnection(url, username, password);
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//            logger.error("Could not connect to db");
//        }
//    }
//
//    public int addAccount(String username, String password, String role) {
//        try {
//            PreparedStatement ps = con.prepareStatement("select count(*) from users where username = ?");
//            ps.setString(1, username);
//            ResultSet rs = ps.executeQuery();
//
//            rs.next();
//            if(rs.getInt("count") > 0) {
//                return 0;
//            }
//            else {
//                ps = con.prepareStatement("insert into users values (default, ?, ?, ?)");
//
//                ps.setString(1, username);
//                ps.setString(2, password);
//                if(role.equals("doctor"))
//                    ps.setInt(3, 1);
//                else if(role.equals("patient"))
//                    ps.setInt(3, 2);
//                ps.execute();
//
//                ps = con.prepareStatement("select id from users where username = ?");
//                ps.setString(1,username);
//                rs = ps.executeQuery();
//                rs.next();
//
//                return rs.getInt(1);
//            }
//        }
//        catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        return 0;
//    }
//
//    public User getUser(String username, String password) {
//        User user = null;
//        try {
//            PreparedStatement ps = con.prepareStatement("select * from users where username = ?");
//            ps.setString(1, username);
//            ResultSet rs = ps.executeQuery();
//
//            if (rs.next()) {
//                String resUsername = rs.getString("username");
//                String resPassword = rs.getString("password");
//                if(!resPassword.equals(password))
//                    throw new BadRequestResponse("Password incorrect");
//                user = new User(
//                        rs.getInt("id"),
//                        resUsername,
//                        resPassword,
//                        rs.getInt("role")
//                );
//            }
//            else {
//                logger.warn("no user found");
//                throw new NotFoundResponse("No user found");
//            }
//        }
//        catch(SQLException se) {
//            se.printStackTrace();
//        }
//        return user;
//    }
//
//    public boolean addPatient(Patient p) {
//        try {
//            if(patientExists(p.getLastName(), p.getFirstName())) {
//                throw new BadRequestResponse("Patient already exists");
//            }
//
//            PreparedStatement ps = con.prepareStatement(
//                    "insert into patients values " +
//                            "(default,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
//            ps.setInt(1, p.getUid());
//            ps.setString(2, p.getFirstName());
//            ps.setString(3, p.getLastName());
//            ps.setString(4, p.getSex());
//            ps.setString(5, p.getDOB());
//            ps.setString(6, p.getRace());
//            ps.setString(7, p.getAddress());
//            ps.setString(8, p.getCity());
//            ps.setString(9, p.getState());
//            ps.setString(10, p.getPhone());
//            ps.setString(11, p.getInsuranceName());
//            ps.setInt(12, p.getInsurancePolicyNumber());
//            ps.setInt(13, p.getInsuranceGroupID());
//            ps.execute();
//
//            return true;
//        } catch (SQLException se) {
//            se.printStackTrace();
//        }
//        return false;
//    }
//
//    public List<Patient> searchPatients(String mode, String param1, String param2) {
//        LinkedList<Patient> list = new LinkedList<>();
//        try {
//            ResultSet rs = null;
//            if(mode.equals("all")) {
//                Statement s = con.createStatement();
//                rs = s.executeQuery("select * from patients");
//            }
//            else if(mode.equals("name")) {
//                PreparedStatement ps = con.prepareStatement("select * from patients where " +
//                        "lastName = ? and firstName = ?");
//                ps.setString(1, param1);
//                ps.setString(2, param2);
//                rs = ps.executeQuery();
//            }
//            else if(mode.equals("id")) {
//                PreparedStatement ps = con.prepareStatement("select * from patients where uid = ?");
//                ps.setInt(1, Integer.parseInt(param1));
//                rs = ps.executeQuery();
//            }
//
//            while(rs.next()) {
//                list.add(new Patient(
//                        rs.getInt("id"),
//                        rs.getInt("uid"),
//                        rs.getString("firstName"),
//                        rs.getString("lastName"),
//                        rs.getString("sex"),
//                        rs.getString("DOB"),
//                        rs.getString("race"),
//                        rs.getString("address"),
//                        rs.getString("city"),
//                        rs.getString("state"),
//                        rs.getString("phone"),
//                        rs.getString("insuranceName"),
//                        rs.getInt("insurancePolicyNumber"),
//                        rs.getInt("insuranceGroupID")
//                ));
//            }
//        }
//        catch(SQLException se) {
//            se.printStackTrace();
//        }
//
//        return list;
//    }
//
//    public boolean editPatient(String lastName, String firstName, String field, String newValue, int uid) {
//
//        try {
//            PreparedStatement ps = con.prepareStatement("select uid from patients where " +
//                    "lastName = ? and firstName = ?");
//            ps.setString(1, lastName);
//            ps.setString(2, firstName);
//            ResultSet rs = ps.executeQuery();
//            if(!rs.next())
//                throw new BadRequestResponse("Patient not found");
//            else {
//                if(rs.getInt(1) != uid) {
//                    ps = con.prepareStatement("select id from users where id = ? and role = 1");
//                    ps.setInt(1, uid);
//                    rs = ps.executeQuery();
//                    if(!rs.next())
//                        throw new UnauthorizedResponse("This request requires admin privileges");
//                }
//            }
//
//            ps = con.prepareStatement("update patients set "+field+" = ? where " +
//                    "lastName = ? and firstName = ?");
//            switch(field) {
//                case "insurancePolicyNumber":
//                case "insuranceGroupID":
//                    ps.setInt(1, Integer.parseInt(newValue));
//                    break;
//                default:
//                    ps.setString(1, newValue);
//            }
//            ps.setString(2, lastName);
//            ps.setString(3, firstName);
//            return ps.executeUpdate() > 0;
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//            return false;
//        }
//    }
//
//    public boolean deletePatient(String lastName, String firstName) {
//        try {
//            PreparedStatement ps = con.prepareStatement("delete from patients where " +
//                    "lastName = ? and firstName = ?");
//            ps.setString(1, lastName);
//            ps.setString(2, firstName);
//            return ps.executeUpdate() > 0;
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//            return false;
//        }
//    }
//
//    private boolean patientExists(String lastName, String firstName) {
//        try {
//            PreparedStatement ps = con.prepareStatement("select count(*) from patients " +
//                    "where lastName = ? and firstName = ?");
//            ps.setString(1, lastName);
//            ps.setString(2, firstName);
//            ResultSet rs = ps.executeQuery();
//            rs.next();
//            if(rs.getInt(1) > 0)
//                return true;
//            return false;
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//            return true;
//        }
//    }
//}
